.. _user_guide:

User Guides
===========

.. toctree::
   :maxdepth: 3
   :name: toc-userguide
   
   media.rst
   playback.rst
   audio.rst
   video.rst
   subtitles.rst
   hotkeys.rst

   
